FROM kibana:5

RUN /usr/share/kibana/bin/kibana-plugin install x-pack && \
  chown -R kibana.kibana /usr/share/kibana/* && \
  echo "xpack.security.enabled: false" >> /etc/kibana/kibana.yml && \
  echo "xpack.graph.enabled: false" >> /etc/kibana/kibana.yml && \
  echo "xpack.reporting.enabled: false" >> /etc/kibana/kibana.yml
